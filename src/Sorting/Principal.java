/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sorting;

import Sorting.OrdenarDados;

public class Principal {

    public static void main(String[] args) {
        OrdenarDados ordenar = new OrdenarDados(30);
        
        ordenar.mostraVetor();
        ordenar.partitionSort(0, ordenar.getTamanho()-1);     
        ordenar.mostraVetor();
    }
    
}
