
package Sorting;

import java.util.Random;

public class OrdenarDados {
    
    private int vetor[];
    private int tamanho;
    
    public OrdenarDados(int tamanho){
        vetor = new int[tamanho];

        this.tamanho = tamanho;
        povoarVetor();
    }
    
    private void povoarVetor(){
        Random gerador = new Random();
        for(int i = 0; i < tamanho; i++){
            vetor[i] = gerador.nextInt(312312);
        }
    }
    
    public void bubbleSort() 
    { 
        for (int i = 0; i < tamanho-1; i++) 
            for (int j = 0; j < tamanho-i-1; j++) 
                if (vetor[j] > vetor[j+1]) 
                { 
                    // swap arr[j+1] and arr[i] 
                    int temp = vetor[j]; 
                    vetor[j] = vetor[j+1]; 
                    vetor[j+1] = temp; 
                } 
    } 

    public void insertionSort() 
    { 
        ; 
        for (int i = 1; i < tamanho; ++i) { 
            int key = vetor[i]; 
            int j = i - 1; 
  
            /* Move elements of arr[0..i-1], that are 
               greater than key, to one position ahead 
               of their current position */
            while (j >= 0 && vetor[j] > key) { 
                vetor[j + 1] = vetor[j]; 
                j = j - 1; 
            } 
            vetor[j + 1] = key; 
        } 
    } 

    public void selectionSort() 
    { 

        // One by one move boundary of unsorted subarray 
        for (int i = 0; i < tamanho-1; i++) 
        { 
            // Find the minimum element in unsorted array 
            int min_idx = i; 
            for (int j = i+1; j < tamanho; j++) 
                if (vetor[j] < vetor[min_idx]) 
                    min_idx = j; 
  
            // Swap the found minimum element with the first 
            // element 
            int temp = vetor[min_idx]; 
            vetor[min_idx] = vetor[i]; 
            vetor[i] = temp; 
        } 
    } 
    
    private void merge(int l, int m, int r) 
    { 
        // Find sizes of two subvetorays to be merged 
        int n1 = m - l + 1; 
        int n2 = r - m; 
  
        /* Create temp vetorays */
        int L[] = new int [n1]; 
        int R[] = new int [n2]; 
  
        /*Copy data to temp vetorays*/
        for (int i=0; i<n1; ++i) 
            L[i] = vetor[l + i]; 
        for (int j=0; j<n2; ++j) 
            R[j] = vetor[m + 1+ j]; 
  
  
        /* Merge the temp vetorays */
  
        // Initial indexes of first and second subvetorays 
        int i = 0, j = 0; 
  
        // Initial index of merged subvetory vetoray 
        int k = l; 
        while (i < n1 && j < n2) 
        { 
            if (L[i] <= R[j]) 
            { 
                vetor[k] = L[i]; 
                i++; 
            } 
            else
            { 
                vetor[k] = R[j]; 
                j++; 
            } 
            k++; 
        } 
  
        /* Copy remaining elements of L[] if any */
        while (i < n1) 
        { 
            vetor[k] = L[i]; 
            i++; 
            k++; 
        } 
  
        /* Copy remaining elements of R[] if any */
        while (j < n2) 
        { 
            vetor[k] = R[j]; 
            j++; 
            k++; 
        } 
    } 
  
    // Main function that sorts vetor[l..r] using 
    // merge() 
    public void mergeSort(int l, int r) 
    { 
        if (l < r) 
        { 
            // Find the middle point 
            int m = (l+r)/2; 
  
            // Sort first and second halves 
            mergeSort(l, m); 
            mergeSort(m+1, r); 
  
            // Merge the sorted halves 
            merge(l, m, r); 
        } 
    } 
    
    private int partition(int low, int high) 
    { 
        int pivot = vetor[high];  
        int i = (low-1); // index of smaller element 
        for (int j=low; j<high; j++) 
        { 
            // If current element is smaller than or 
            // equal to pivot 
            if (vetor[j] <= pivot) 
            { 
                i++; 
  
                // swap vetor[i] and vetor[j] 
                int temp = vetor[i]; 
                vetor[i] = vetor[j]; 
                vetor[j] = temp; 
            } 
        } 
  
        // swap vetor[i+1] and vetor[high] (or pivot) 
        int temp = vetor[i+1]; 
        vetor[i+1] = vetor[high]; 
        vetor[high] = temp; 
  
        return i+1; 
    } 
  
  
    /* The main function that implements QuickSort() 
      vetor[] --> Array to be sorted, 
      low  --> Starting index, 
      high  --> Ending index */
    public void partitionSort(int low, int high) 
    { 
        if (low < high) 
        { 
            /* pi is partitioning index, vetor[pi] is  
              now at right place */
            int pi = partition(low, high); 
  
            // Recursively sort elements before 
            // partition and after partition 
            partitionSort(low, pi-1); 
            partitionSort(pi+1, high); 
        } 
    }
    
    public void shellSort() 
    { 
        int n = vetor.length; 
  
        // Start with a big gap, then reduce the gap 
        for (int gap = n/2; gap > 0; gap /= 2) 
        { 
            // Do a gapped insertion sort for this gap size. 
            // The first gap elements a[0..gap-1] are already 
            // in gapped order keep adding one more element 
            // until the entire vetoray is gap sorted 
            for (int i = gap; i < n; i += 1) 
            { 
                // add a[i] to the elements that have been gap 
                // sorted save a[i] in temp and make a hole at 
                // position i 
                int temp = vetor[i]; 
  
                // shift earlier gap-sorted elements up until 
                // the correct location for a[i] is found 
                int j; 
                for (j = i; j >= gap && vetor[j - gap] > temp; j -= gap) 
                    vetor[j] = vetor[j - gap]; 
  
                // put temp (the original a[i]) in its correct 
                // location 
                vetor[j] = temp; 
            } 
        } 
    } 

    public void mostraVetor(){
        String lista = new String();
        for(int i = 0; i < tamanho; i++){            
            if (i == 0){
                lista = "" + vetor[i];
            } else {
                lista = lista + ", " + vetor[i];
            }
        }
        System.out.println(lista);
    }
    
    public int getTamanho(){
        return tamanho;
    }
    
    public int buscaSequencialOrdenada (int x){
        for(int i = 0; i < tamanho; i++){
            if (vetor[i] <= x && vetor[i] == x){
                return i;
            }
        }
        return -1;    
    } 
    public int buscaSequencial (int x){
        for(int i = 0; i < tamanho; i++){
            if (vetor[i] == x){
                return i;
            }
        }
        return -1;    
    }
    public int buscaBinaria (int x){
        int l = 0, r = tamanho-1, m = 0;
        
        while ( l <= r) {
            m = (l+r)/2;
            if (vetor[m] == x){
                return m;
            }
            if (vetor[m] < x){
                l = m + 1;
            } else{
                r = m-1;
            }
        } 
        return -1;
    }
    
}
